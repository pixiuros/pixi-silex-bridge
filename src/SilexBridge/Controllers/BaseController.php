<?php

namespace Pixi\SilexBridge\Controllers;

use Silex\Application;
use Silex\ControllerProviderInterface;

abstract class BaseController implements ControllerProviderInterface
{
    use \Pixi\SilexBridge\AppTrait;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $db;

    /**
     * @var \Doctrine\DBAL\Connection
     */
    protected $customerdb;

    /**
     * @var \Pixi\API\Soap\Client
     */
    protected $pixiapi;

    public function __construct(Application $app)
    {
        $this->setApp($app);

        $this->db         = $app['dbs']['db'];
        $this->customerdb = $app['dbs']['customerdb'];

        $this->pixiapi = $app['pixiapi'];
    }

    public function connect(Application $app)
    {
        $ns      = get_class($this);
        $bind    = sprintf("app.%s", $ns);
        $service = sprintf("controller.app.%s", $ns);

        return (object) [
            'service' => $service,
            'bind'    => $bind,
        ];
    }
}
