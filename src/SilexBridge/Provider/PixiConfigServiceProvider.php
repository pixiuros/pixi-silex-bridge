<?php

namespace Pixi\SilexBridge\Provider;

use Silex\Application;
use Silex\ServiceProviderInterface;

class PixiConfigServiceProvider implements ServiceProviderInterface
{
    public function register(Application $app)
    {
        $cfg = require_once BASE_PATH . '/config/pixi.php';

        // setup db settings from session
        if (isset($_SESSION['ENV']['db'])) {
            $cfg['dbs']['db']['host']     = $_SESSION['ENV']['db']['hostname'];
            $cfg['dbs']['db']['dbname']   = $_SESSION['ENV']['db']['database'];
            $cfg['dbs']['db']['user']     = $_SESSION['ENV']['db']['username'];
            $cfg['dbs']['db']['password'] = $_SESSION['ENV']['db']['password'];
            $cfg['dbs']['db']['charset']  = $_SESSION['ENV']['db']['char_set'];
        }

        // setup customerdb settings from session
        if (isset($_SESSION['ENV']['customer_db'])) {
            $cfg['dbs']['customerdb']['host']     = $_SESSION['ENV']['customer_db']['hostname'];
            $cfg['dbs']['customerdb']['dbname']   = $_SESSION['ENV']['customer_db']['database'];
            $cfg['dbs']['customerdb']['user']     = $_SESSION['ENV']['customer_db']['username'];
            $cfg['dbs']['customerdb']['password'] = $_SESSION['ENV']['customer_db']['password'];
            $cfg['dbs']['customerdb']['charset']  = $_SESSION['ENV']['customer_db']['char_set'];
        }

        // setup pixiapi settings from session
        if (isset($_SESSION['accessdata'])) {
            $cfg['pixiapi']['username'] = $_SESSION['accessdata']['username'];
            $cfg['pixiapi']['password'] = $_SESSION['accessdata']['password'];
            $cfg['pixiapi']['location'] = $_SESSION['accessdata']['endpoint'];
            $cfg['pixiapi']['uri']      = $_SESSION['accessdata']['namespace'];
        }

        // db config
        $app['dbs.options'] = $cfg['dbs'];

        // pixi api config
        $app['pixiapi.config'] = $cfg['pixiapi'];

    }

    public function boot(Application $app)
    {
        //
    }

}
